# Raft
> CS443 Lab2: Log Replication
## Data structures

### Global constants
```go
const (
  FOLLOWER  = 1
  CANDIDATE = 2
  LEADER    = 3
)
```
### Struct for states of Raft
```go
type Raft struct {
  mu        sync.Mutex          // Lock to protect shared access to this peer's state
  peers     []*labrpc.ClientEnd // RPC end points of all peers
  persister *Persister          // Object to hold this peer's persisted state
  me        int                 // this peer's index into peers[]
    
  // state of Raft node
  state int // follower 0, candidate 1, follower 2
  
  // persistent state
  currentTerm int            // latest term server has seen (initialized to 0 on first boot, increases monotonically)
  votedFor    int            // candidateId that received vote in current term (or null if none)
  log         []*interface{} // log entries; each entry contains command for state machine, and term when when entry was received by leader (first index is 1)
  
  // volatile state
  commitIndex int // index of highest log entry known to be committed (initialized to 0, increases monotonically)
  lastApplied int // index of hightest log entry applied to state machine (initialized to 0, increases monotonically)
  
  // volatile state for a leader
  nextIndex  []int // for each server, index of the next log entry to send to that server (initialized to leader last log index +1)
  matchIndex []int // for each server, index of highest log entry known to be replicated on server (initialized to 0, increases monotonically)
  
  // states for implementation
  startCount int64 // to count election timeout
}
```
### Structs for RequestVote
```go
type RequestVoteArgs struct {
  Term         int // candidate's term
  CandidateId  int // candidate requesting vote
  LastLogIndex int // index of candidate's last log entry
  LastLogTerm  int // term of candidate's last log entry
}

type RequestVoteReply struct {
  Term        int  // currentTerm, for candidate to update itself
  VoteGranted bool // true means candidate received vote
}
```
### Structs for AppendEntries
```go
type AppendEntriesArgs struct {
  Term int // leader's term
}

type AppendEntriesReply struct {
  Term int // currentTerm, for leader to update itself
}

type Log struct {
  Index   int         // log entry index
  Command interface{} // requested command
  Term    int         // start term
}
```
### Three global goroutines for leader election
```go
// ...
go rf.Election()
go rf.Heartbeat()
go rf.ApplyMessage(applyCh)
// ...
```
### Election timeout
```go
func (rf *Raft) Election() {
  for {
    if state == FOLLOWER {
      // Count for ramdomly between 1sec to 2sec.
      // If time out, Raft be a CANDIDATE.
    } else if state == CANDIDATE {
      // Count for randomly between 500ms to 1000ms.
      // Simaltaneously, send out ReqeustVote.
      // If Raft get a majority of the vote, be a LEADER.
      // Otherwise, be a FOLLOWER and restart above for-loop.
    } else { // LEADER
      // Sleep in 1sec.
    }
  }
}
```
### Heartbeat
```go
func (rf *Raft) Heartbeat() {
  for {
    if state == LEADER {
      // Send out AppendEntries.
      // Sleep in 200ms.
    } else { // FOLLOWER, CANDIDATE
      // Sleep in 1sec.
    }
  }
}
```
### ApplyMsg
```go
func (rf *Raft) ApplyMessage(applyCh chan ApplyMsg) {
  for {
    // Take a rest
    if rf.lastApplied == rf.commitIndex {
      // Ignore
      continue
    }
    // Count how much should we apply
    // send Command to the channel, applyCh
  }
}
```
### RequestVote Handler
```go
func (rf *Raft) RequestVote(args *RequestVoteArgs, reply *RequestVoteReply) {
  // If higher term is detected, be a follower and set currentTerm to args.Term
  // If less term is detected, reply false
  // If I'm more up-to-date, reply false, otherwise reply true
  // If vote granted, be a follower, set currentTerm to args.Term, and set startCount to 0
  // Reply currentTerm
}
```
### AppendEntries Handler
```go
func (rf *Raft) AppendEntries(args *AppendEntriesArgs, reply *AppendEntriesReply) {
  // Reset Raft.startCount to 0.
  // If higher term is detected, set currentTerm to the higher term and be a FOLLOWER.
  // If less term is detected, reply false
  // If log doesn’t contain an entry at prevLogIndex whose term matches prevLogTerm, reply false
  // Add new entry in log and reply true
  // Reply currentTerm
}
```
### Raft.GetState
```go
func (rf *Raft) GetState() (int, bool) {
  var term int
  var isleader bool
  rf.mu.Lock()
  term = rf.currentTerm
  if rf.state == LEADER {
    isleader = true
  }
  rf.mu.Unlock()
  return term, isleader
}
```
### Raft.Start
```go
func (rf *Raft) Start(command interface{}) (int, int, bool) {
  index := -1
  term := -1
  isLeader := true

  rf.mu.Lock()
  if rf.state != LEADER {
    isLeader = false
    term = rf.currentTerm
    rf.mu.Unlock()
    return index, term, isLeader
  }
  // LEADER
  log := Log{}
  log.Command = command
  log.Index = len(rf.log) + 1
  log.Term = rf.currentTerm
  rf.log = append(rf.log, log)
  rf.nextIndex[rf.me]++
  rf.matchIndex[rf.me]++
  index = log.Index
  term = rf.currentTerm
  rf.mu.Unlock()
  return index, term, isLeader
}
```