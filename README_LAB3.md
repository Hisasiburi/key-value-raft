# Raft
> CS443 Lab3: Fault-tolerant Key/Value Service
## Constants & Data structures
### common.go
#### Constants of Error
To identify execution results of Get RPC & PutAppend RPC
```go
const (
  OK        = "OK"
  ErrNoKey  = "ErrNoKey"
  NotLeader = "NotLeader"
  Duplicate = "Duplicate"
)
```
#### Struct of PutAppend RPC
RPC message of 'Put' or 'Append' requests
```go
type PutAppendArgs struct {
  Key   string
  Value string
  Op    string
  ClientId    int64  // give uniqueness to each client
  OperationId int64  // give uniqueness to each operation
}
```
```go
type PutAppendReply struct {
  WrongLeader bool
  Err         Err
}
```
#### Struct of Get RPC
RPC message of 'Get' request
```go
type GetArgs struct {
  Key string
  ClientId    int64  // give uniqueness to each client
  OperationId int64  // give uniqueness to each operation
}
```
```go
type GetReply struct {
  WrongLeader bool
  Err         Err
  Value       string
}
```
### client.go
#### Struct of Clerk
```go
type Clerk struct {
  servers []*labrpc.ClientEnd
  me       int64       // To identify each client
  mu       sync.Mutex  // To ensure "one client call at a time"
  leaderId int         // To redirect server of failed 'Get' or 'PutAppend' RPC
}
```
### server.go
#### Struct of KVServer
```go
type KVServer struct {
  mu      sync.Mutex
  me      int
  rf      *raft.Raft
  applyCh chan raft.ApplyMsg
  maxraftstate int
  store   []Record  // To store key-value data
  results []Result  // To store sequential results of linearized operations
}
```
#### Struct of Operation
Argument of Raft.Start()
```go
type Op struct {
  Oid int64  // To ensure uniqueness of each operation (OperationId)
  Fun string // Type of operation
  Key string // key argument
  Val string // value argument
}
```
#### Element of KVServer.store
Store values corresponding each key.
```go
type Record struct {
  key string
  val string
}
```
#### Element of KVServer.results
Store execution results of each request.
```go
type Result struct {
  oid int64  // To check out the existing operation id in results
  idx int // committed index of corresponding result
  trm int // commiteed term of corresponding result
  fun string  // type of operation
  val string  // returned value
  err bool  // type of error
  dup bool  // whether the operation was duplicate or not
}
```

## Core Fuctions
### client.go
#### Client's 'Get' request
```go
func (ck *Clerk) Get(key string) string
```
1. Send a RPC to a server.
1. If not ok of the reponse, send again to another server.
1. Keep request until a leader responds OK.
1. Return a successful value.
#### Client's 'PutAppend' request
```go
func (ck *Clerk) PutAppend(key string, value string, op string)
```
1. Send a RPC to a server.
1. If not ok of the reponse, send again to another server.
1. Keep request until a leader responds OK.
### server.go
#### Server's 'Get' request handler
```go
func (kv *KVServer) Get(args *GetArgs, reply *GetReply)
```
1. Start given 'Get' operation through Raft.Start().
1. Wait until end of the operation.
1. Check out duplication & leader change.
1. If it has a problem, reply an error.
1. If not, reply a value.
#### Server's 'PutAppend' request handler
```go
func (kv *KVServer) PutAppend(args *PutAppendArgs, reply *PutAppendReply)
```
1. Start given 'PutAppend' operation through Raft.Start().
1. Wait until end of the operation.
1. Check out duplication & leader change.
1. If it has a problem, reply an error.
1. If not, done.
#### Compute linearized operations in StartKVServer() (goroutine)
1. Receive a ApplyMsg through applyCh which has an operation.
1. Compute the operation, which is linearized.
1. Apply it into the KVServer.store.
1. Return value into the KVServer.results.