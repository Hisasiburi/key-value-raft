package raft

//
// this is an outline of the API that raft must expose to
// the service (or tester). see comments below for
// each of these functions for more details.
//
// rf = Make(...)
//   create a new Raft server.
// rf.Start(command interface{}) (index, term, isleader)
//   start agreement on a new log entry
// rf.GetState() (term, isLeader)
//   ask a Raft for its current term, and whether it thinks it is leader
// ApplyMsg
//   each time a new entry is committed to the log, each Raft peer
//   should send an ApplyMsg to the service (or tester)
//   in the same server.
//

import (
	"labrpc"
	"math/rand"
	"sort"
	"sync"
	"time"
)

// import "bytes"
// import "labgob"

//
// as each Raft peer becomes aware that successive log entries are
// committed, the peer should send an ApplyMsg to the service (or
// tester) on the same server, via the applyCh passed to Make(). set
// CommandValid to true to indicate that the ApplyMsg contains a newly
// committed log entry.
//
// in Lab 3 you'll want to send other kinds of messages (e.g.,
// snapshots) on the applyCh; at that point you can add fields to
// ApplyMsg, but set CommandValid to false for these other uses.
//
type ApplyMsg struct {
	CommandValid bool
	Command      interface{}
	CommandIndex int
	CommandTerm  int
}

const (
	FOLLOWER  = 1
	CANDIDATE = 2
	LEADER    = 3
)

//
// A Go object implementing a single Raft peer.
//
type Raft struct {
	mu        sync.Mutex          // Lock to protect shared access to this peer's state
	peers     []*labrpc.ClientEnd // RPC end points of all peers
	persister *Persister          // Object to hold this peer's persisted state
	me        int                 // this peer's index into peers[]

	// Your data here (Lab1, Lab2, Challenge1).
	// Look at the paper's Figure 2 for a description of what
	// state a Raft server must maintain.
	applyCh chan ApplyMsg
	state   int // follower 0, candidate 1, follower 2
	// persistent state
	currentTerm int   // latest term server has seen (initialized to 0 on first boot, increases monotonically)
	votedFor    int   // candidateId that received vote in current term (or null if none)
	log         []Log // log entries; each entry contains command for state machine, and term when entry was received by leader (first index is 1)
	// volatile state
	commitIndex int // index of highest log entry known to be committed (initialized to 0, increases monotonically)
	lastApplied int // index of highest log entry applied to state machine (initialized to 0, increases monotonically)
	// volatile state for a leader
	nextIndex  []int // for each server, index of the next log entry to send to that server (initialized to leader last log index +1)
	matchIndex []int // for each server, index of highest log entry known to be replicated on server (initialized to 0, increases monotonically)
	// states for implementation
	startCount int64 // to count election timeout
}

// For debugging
func (rf *Raft) print(field string, value interface{}) {
	// var state string
	// switch rf.state {
	// case FOLLOWER:
	// 	state = "FOLLOWER  "
	// case CANDIDATE:
	// 	state = "CANDIDATE "
	// case LEADER:
	// 	state = "LEADER    "
	// }
	// fmt.Print("[", state, rf.me, " #", rf.currentTerm, "] ", field, " = ", value, "\n")
}

// return currentTerm and whether this server
// believes it is the leader.
func (rf *Raft) GetState() (int, bool) {

	var term int
	var isleader bool
	// Your code here (Lab1).
	rf.mu.Lock()
	term = rf.currentTerm
	if rf.state == LEADER {
		isleader = true
	}
	rf.mu.Unlock()
	return term, isleader
}

//
// save Raft's persistent state to stable storage,
// where it can later be retrieved after a crash and restart.
// see paper's Figure 2 for a description of what should be persistent.
//
func (rf *Raft) persist() {
	// Your code here (Challenge1).
	// Example:
	// w := new(bytes.Buffer)
	// e := labgob.NewEncoder(w)
	// e.Encode(rf.xxx)
	// e.Encode(rf.yyy)
	// data := w.Bytes()
	// rf.persister.SaveRaftState(data)
}

//
// restore previously persisted state.
//
func (rf *Raft) readPersist(data []byte) {
	if data == nil || len(data) < 1 { // bootstrap without any state?
		return
	}
	// Your code here (Challenge1).
	// Example:
	// r := bytes.NewBuffer(data)
	// d := labgob.NewDecoder(r)
	// var xxx
	// var yyy
	// if d.Decode(&xxx) != nil ||
	//    d.Decode(&yyy) != nil {
	//   error...
	// } else {
	//   rf.xxx = xxx
	//   rf.yyy = yyy
	// }
}

//
// example RequestVote RPC arguments structure.
// field names must start with capital letters!
//
type RequestVoteArgs struct {
	// Your data here (Lab1, Lab2).
	Term         int // candidate's term
	CandidateId  int // candidate requesting vote
	LastLogIndex int // index of candidate's last log entry
	LastLogTerm  int // term of candidate's last log entry
}

//
// example RequestVote RPC reply structure.
// field names must start with capital letters!
//
type RequestVoteReply struct {
	// Your data here (Lab1).
	Term        int  // currentTerm, for candidate to update itself
	VoteGranted bool // true means candidate received vote
}

func (rf *Raft) IsMoreUpToDate(lastLogIndex int, lastLogTerm int) bool {
	if rf.log[len(rf.log)-1].Term > lastLogTerm {
		return true
	}
	if rf.log[len(rf.log)-1].Term == lastLogTerm && len(rf.log) > lastLogIndex {
		return true
	}
	return false
}

//
// example RequestVote RPC handler.
//
func (rf *Raft) RequestVote(args *RequestVoteArgs, reply *RequestVoteReply) {
	// Your code here (Lab1, Lab2).
	rf.mu.Lock()
	reply.VoteGranted = true
	if args.Term > rf.currentTerm {
		rf.state = FOLLOWER
		rf.currentTerm = args.Term
		rf.votedFor = -1
	}
	if args.Term < rf.currentTerm {
		reply.VoteGranted = false
	}
	// if args.LastLogIndex > 0 {
	if args.LastLogIndex > 0 && (rf.votedFor == -1 || rf.votedFor == args.CandidateId) {
		if rf.IsMoreUpToDate(args.LastLogIndex, args.LastLogTerm) {
			reply.VoteGranted = false
		} else {
			reply.VoteGranted = true
		}
	}
	if reply.VoteGranted {
		rf.state = FOLLOWER
		rf.votedFor = args.CandidateId
		// rf.currentTerm = args.Term
		rf.startCount = 0
	}
	reply.Term = rf.currentTerm
	rf.mu.Unlock()
}

type AppendEntriesArgs struct {
	Term         int   // leader's term
	LeaderId     int   // so follower can redirect clients
	PrevLogIndex int   // index of log entry immediately preceding new ones
	PrevLogTerm  int   // term of PrevLogIndex entry
	Entries      []Log // log entries to store (empty for heartbeat; may send more than one for efficiency)
	LeaderCommit int   // leader's commit index
}

type AppendEntriesReply struct {
	Term          int  // currentTerm, for leader to update itself
	Success       bool // true if follower contained entry matching PrevLogIndex and PrevLogTerm
	ConflictIndex int
	ConflictTerm  int
}

func FindConflictIndex(logs []Log, term int) int {
	for i := 0; i < len(logs); i++ {
		if logs[i].Term == term {
			continue
		}
		return i + 1
	}
	return 0
}

func (rf *Raft) AppendEntries(args *AppendEntriesArgs, reply *AppendEntriesReply) {
	rf.mu.Lock()
	Term := args.Term
	PrevLogIndex := args.PrevLogIndex
	rf.print("PrevLogIndex", PrevLogIndex)
	PrevLogTerm := args.PrevLogTerm
	rf.print("PrevLogTerm", PrevLogTerm)
	Entries := args.Entries
	rf.print("Entries", Entries)
	LeaderCommit := args.LeaderCommit
	rf.print("rf.log", rf.log)
	// leader election
	rf.startCount = 0
	reply.Success = true
	reply.ConflictIndex = 0
	reply.ConflictTerm = 0
	if Term > rf.currentTerm {
		rf.state = FOLLOWER
		rf.currentTerm = Term
		rf.votedFor = -1
	}
	// log replication
	if Term < rf.currentTerm {
		reply.Success = false
		reply.Term = rf.currentTerm
		rf.print("Term < rf.currentTerm REPLY:", reply.Success)
		rf.mu.Unlock()
		return
	}
	if PrevLogIndex != 0 && PrevLogIndex > len(rf.log) {
		reply.Success = false
		reply.Term = rf.currentTerm
		reply.ConflictIndex = len(rf.log)
		if reply.ConflictIndex > 0 {
			reply.ConflictTerm = rf.log[reply.ConflictIndex-1].Term
		}
		rf.print("PrevLogIndex", PrevLogIndex)
		rf.print("len(rf.log)", len(rf.log))
		rf.print("PrevLogIndex != 0 && PrevLogIndex > len(rf.log) REPLY:", reply.Success)
		rf.mu.Unlock()
		return
	}
	// log replication
	if len(rf.log) != 0 && PrevLogIndex > 0 && PrevLogIndex <= len(rf.log) && (rf.log[PrevLogIndex-1].Term != PrevLogTerm) {
		reply.Success = false
		reply.Term = rf.currentTerm
		reply.ConflictIndex = FindConflictIndex(rf.log, PrevLogTerm)
		if reply.ConflictIndex > 0 {
			reply.ConflictTerm = rf.log[reply.ConflictIndex-1].Term
		}
		rf.print("rf.log[PrevLogIndex-1].Term", rf.log[PrevLogIndex-1].Term)
		rf.print("PrevLogTerm", PrevLogTerm)
		rf.print("len(rf.log) != 0 && PrevLogIndex > 0 && PrevLogIndex <= len(rf.log) && (rf.log[PrevLogIndex-1].Term != PrevLogTerm) REPLY:", reply.Success)
		rf.mu.Unlock()
		return
	}
	for i := 0; i < len(Entries); i++ {
		receivedLog := Entries[i]
		if receivedLog.Index <= len(rf.log) && rf.log[receivedLog.Index-1].Term != receivedLog.Term {
			rf.log = rf.log[:receivedLog.Index-1]
			break
		}
	}
	for j := 0; j < len(Entries); j++ {
		receivedLog := Entries[j]
		if receivedLog.Index > len(rf.log) {
			newLog := Log{}
			newLog.Command = receivedLog.Command
			newLog.Term = receivedLog.Term
			newLog.Index = len(rf.log) + 1
			rf.log = append(rf.log, newLog)
		}
	}
	if LeaderCommit > rf.commitIndex {
		rf.commitIndex = func(a int, b int) int {
			if a < b {
				return a
			}
			return b
		}(LeaderCommit, len(rf.log))
	}
	reply.Term = rf.currentTerm
	rf.print("SUCCESS REPLY:", reply.Success)
	rf.mu.Unlock()
}

//
// example code to send a RequestVote RPC to a server.
// server is the index of the target server in rf.peers[].
// expects RPC arguments in args.
// fills in *reply with RPC reply, so caller should
// pass &reply.
// the types of the args and reply passed to Call() must be
// the same as the types of the arguments declared in the
// handler function (including whether they are pointers).
//
// The labrpc package simulates a lossy network, in which servers
// may be unreachable, and in which requests and replies may be lost.
// Call() sends a request and waits for a reply. If a reply arrives
// within a timeout interval, Call() returns true; otherwise
// Call() returns false. Thus Call() may not return for a while.
// A false return can be caused by a dead server, a live server that
// can't be reached, a lost request, or a lost reply.
//
// Call() is guaranteed to return (perhaps after a delay) *except* if the
// handler function on the server side does not return.  Thus there
// is no need to implement your own timeouts around Call().
//
// look at the comments in ../labrpc/labrpc.go for more details.
//
// if you're having trouble getting RPC to work, check that you've
// capitalized all field names in structs passed over RPC, and
// that the caller passes the address of the reply struct with &, not
// the struct itself.
//
func (rf *Raft) sendRequestVote(server int, args *RequestVoteArgs, reply *RequestVoteReply) bool {
	ok := rf.peers[server].Call("Raft.RequestVote", args, reply)
	return ok
}

func (rf *Raft) sendAppendEntries(server int, args *AppendEntriesArgs, reply *AppendEntriesReply) bool {
	ok := rf.peers[server].Call("Raft.AppendEntries", args, reply)
	return ok
}

type Log struct {
	Index   int
	Command interface{}
	Term    int
}

// func (rf *Raft) duplicate(Oid int64) {
// 	for _, elem := range rf.log {
// 		op, ok := elem.Command.(Op)
// 		if op.Oid == Oid {
// 			return true
// 		}
// 	}
// 	return false
// }

//
// the service using Raft (e.g. a k/v server) wants to start
// agreement on the next command to be appended to Raft's log. if this
// server isn't the leader, returns false. otherwise start the
// agreement and return immediately. there is no guarantee that this
// command will ever be committed to the Raft log, since the leader
// may fail or lose an election.
//
// the first return value is the index that the command will appear at
// if it's ever committed. the second return value is the current
// term. the third return value is true if this server believes it is
// the leader.
//
func (rf *Raft) Start(command interface{}) (int, int, bool) {
	index := -1
	term := -1
	isLeader := true

	// Your code here (Lab2).
	rf.mu.Lock()
	if rf.state != LEADER {
		isLeader = false
		term = rf.currentTerm
		rf.mu.Unlock()
		return index, term, isLeader
	}
	// LEADER
	log := Log{}
	log.Command = command
	log.Index = len(rf.log) + 1
	log.Term = rf.currentTerm
	rf.print("START Command", command)
	rf.print("START Term", rf.currentTerm)
	rf.log = append(rf.log, log)
	rf.nextIndex[rf.me] = log.Index + 1
	rf.matchIndex[rf.me] = log.Index
	index = log.Index
	term = rf.currentTerm
	rf.mu.Unlock()
	return index, term, isLeader
}

//
// the tester calls Kill() when a Raft instance won't
// be needed again. you are not required to do anything
// in Kill(), but it might be convenient to (for example)
// turn off debug output from this instance.
//
func (rf *Raft) Kill() {
	// Your code here, if desired.
	rf.mu.Lock()
	rf.print("rf.commitIndex", rf.commitIndex)
	rf.print("Kill", rf.log)
	rf.state = FOLLOWER
	rf.currentTerm = 0
	rf.votedFor = -1
	rf.log = []Log{}
	rf.commitIndex = 0
	rf.lastApplied = 0
	rf.mu.Unlock()
}

func (rf *Raft) Election() {
	prevState := FOLLOWER
	for {
		rf.mu.Lock()
		state := rf.state
		peers := len(rf.peers)
		rf.mu.Unlock()
		if state == FOLLOWER || (prevState == CANDIDATE && state == CANDIDATE) {
			rf.mu.Lock()
			prevState = state
			rf.startCount = 0
			interval := 40 + rand.Int63n(40)
			startCount := rf.startCount
			rf.mu.Unlock()
			for startCount < interval {
				time.Sleep(10 * time.Millisecond)
				rf.mu.Lock()
				rf.startCount++
				startCount = rf.startCount
				rf.mu.Unlock()
			}
			rf.mu.Lock()
			if rf.state == FOLLOWER {
				rf.state = CANDIDATE
				rf.currentTerm++
				rf.votedFor = -1
			}
			prevState = FOLLOWER
			rf.mu.Unlock()
		} else if state == CANDIDATE {
			timeout := make(chan bool)
			rf.mu.Lock()
			prevState = state
			currentTerm := rf.currentTerm
			votes := 1
			agree := 1
			lastLogIndex := 0
			lastLogTerm := 0
			if len(rf.log) > 0 {
				lastLogIndex = len(rf.log)
				lastLogTerm = rf.log[lastLogIndex-1].Term
			}
			go func() {
				var tick int64
				interval := 10 + rand.Int63n(10)
				rf.mu.Lock()
				currentState := rf.state
				currentVote := votes
				rf.mu.Unlock()
				// timeout         // be a FOLLOWER         // get all success
				for tick < interval && currentState == CANDIDATE && currentVote != peers {
					time.Sleep(10 * time.Millisecond)
					tick++
					rf.mu.Lock()
					currentState = rf.state
					currentVote = votes
					rf.mu.Unlock()
				}
				timeout <- true
			}()
			for i := 0; i < peers; i++ {
				if i == rf.me {
					continue
				}
				go func(server int) {
					args := RequestVoteArgs{}
					args.Term = currentTerm
					args.CandidateId = rf.me
					args.LastLogIndex = lastLogIndex
					args.LastLogTerm = lastLogTerm
					reply := RequestVoteReply{}
					success := rf.sendRequestVote(server, &args, &reply)
					rf.mu.Lock()
					if success && reply.VoteGranted {
						agree++
					}
					votes++
					if success && reply.Term > rf.currentTerm {
						rf.state = FOLLOWER
						rf.currentTerm = reply.Term
						rf.votedFor = -1
					}
					rf.mu.Unlock()
				}(i)
			}
			rf.mu.Unlock()
			<-timeout
			rf.mu.Lock()
			if rf.state == FOLLOWER {
				rf.mu.Unlock()
				continue
			}
			if agree > votes/2 && !(agree == 1 && votes == 1) {
				rf.state = LEADER
				rf.nextIndex = make([]int, peers)
				rf.matchIndex = make([]int, peers)
				for i := 0; i < peers; i++ {
					rf.nextIndex[i] = rf.commitIndex + 1
					// rf.nextIndex[i] = len(rf.log) + 1
					rf.matchIndex[i] = 0
				}
			}
			rf.mu.Unlock()
		} else { // LEADER
			prevState = state
			time.Sleep(10 * time.Millisecond)
		}
	}
}

func majority(array []int, length int) int {
	candidates := make([]int, length)
	copy(candidates, array)
	sort.Ints(candidates)
	return candidates[(length-1)/2]
}

func (rf *Raft) Heartbeat() {
	for {
		rf.mu.Lock()
		state := rf.state
		currentTerm := rf.currentTerm
		peers := len(rf.peers)
		commitIndex := rf.commitIndex
		rf.mu.Unlock()
		if state == LEADER {
			timeout := make(chan bool)
			rf.mu.Lock()
			votes := 1
			go func() {
				var tick int64
				interval := 10 + rand.Int63n(10)
				rf.mu.Lock()
				currentState := rf.state
				currentVote := votes
				rf.mu.Unlock()
				// timeout         // be a FOLLOWER              // get all success
				for tick < interval && currentState == LEADER && currentVote != peers {
					time.Sleep(10 * time.Millisecond)
					tick++
					rf.mu.Lock()
					currentState = rf.state
					currentVote = votes
					rf.mu.Unlock()
				}
				timeout <- true
			}()
			for i := 0; i < peers; i++ {
				if i == rf.me {
					continue
				}
				nextIndex := rf.nextIndex[i]
				entries := make([]Log, 0)
				length := 0
				prevLogIndex := 0
				prevLogTerm := 0
				if nextIndex > 0 && len(rf.log) >= nextIndex {
					entries = rf.log[nextIndex-1:]
					length = len(entries)
				}
				if nextIndex > 1 && len(rf.log) >= nextIndex-1 {
					prevLogIndex = nextIndex - 1
					prevLogTerm = rf.log[prevLogIndex-1].Term
				}
				go func(server int) {
					args := AppendEntriesArgs{}
					args.Term = currentTerm
					args.LeaderId = rf.me
					args.PrevLogIndex = prevLogIndex
					args.PrevLogTerm = prevLogTerm
					args.Entries = make([]Log, length)
					copy(args.Entries, entries)
					args.LeaderCommit = commitIndex
					reply := AppendEntriesReply{}
					success := rf.sendAppendEntries(server, &args, &reply)
					rf.mu.Lock()
					votes++
					if !success {
						rf.mu.Unlock()
						return
					}
					if reply.Term > rf.currentTerm {
						rf.state = FOLLOWER
						rf.currentTerm = reply.Term
						rf.votedFor = -1
						rf.mu.Unlock()
						return
					}
					if reply.Success {
						if length > 0 {
							rf.nextIndex[server] = entries[length-1].Index + 1
							rf.matchIndex[server] = entries[length-1].Index
						}
					} else {
						// If AppendEntries fails because of log inconsistency: decrement nextIndex and retry
						// if rf.nextIndex[server] > 0 && length > 0 {
						// 	rf.nextIndex[server]--
						// }
						if reply.ConflictIndex > 0 {
							rf.nextIndex[server] = reply.ConflictIndex
						}
					}
					rf.print("rf.nextIndex", rf.nextIndex)
					rf.mu.Unlock()
				}(i)
			}
			rf.mu.Unlock()
			<-timeout
			rf.mu.Lock()
			if rf.state == FOLLOWER {
				rf.mu.Unlock()
				continue
			}
			N := majority(rf.matchIndex, peers)
			if N > rf.commitIndex && rf.log[N-1].Term == rf.currentTerm {
				rf.commitIndex = N
			}
			rf.mu.Unlock()
			time.Sleep(100 * time.Millisecond)
		} else { // FOLLOWER, CANDIDATE
			time.Sleep(10 * time.Millisecond)
		}
	}
}

func (rf *Raft) ApplyMessage(applyCh chan ApplyMsg) {
	for {
		time.Sleep(10 * time.Millisecond)
		rf.mu.Lock()
		if rf.lastApplied == rf.commitIndex {
			rf.mu.Unlock()
			continue
		}
		count := rf.commitIndex - rf.lastApplied
		start := rf.lastApplied
		for i := 0; i < count; i++ {
			applyMsg := ApplyMsg{}
			applyMsg.CommandValid = true
			applyMsg.CommandIndex = rf.log[start+i].Index
			applyMsg.Command = rf.log[start+i].Command
			applyMsg.CommandTerm = rf.log[start+i].Term
			rf.print("APPLY CommandIndex", applyMsg.CommandIndex)
			rf.print("APPLY Command", applyMsg.Command)
			applyCh <- applyMsg
		}
		rf.lastApplied = rf.commitIndex
		rf.mu.Unlock()
	}
}

//
// the service or tester wants to create a Raft server. the ports
// of all the Raft servers (including this one) are in peers[]. this
// server's port is peers[me]. all the servers' peers[] arrays
// have the same order. persister is a place for this server to
// save its persistent state, and also initially holds the most
// recent saved state, if any. applyCh is a channel on which the
// tester or service expects Raft to send ApplyMsg messages.
// Make() must return quickly, so it should start goroutines
// for any long-running work.
//
func Make(peers []*labrpc.ClientEnd, me int,
	persister *Persister, applyCh chan ApplyMsg) *Raft {
	rf := &Raft{}
	rf.peers = peers
	rf.persister = persister
	rf.me = me
	// rf.applyCh = applyCh

	servers := len(rf.peers)
	// Your initialization code here (Lab1, Lab2, Challenge1).
	rf.startCount = 0

	// initialize from state persisted before a crash
	rf.readPersist(persister.ReadRaftState())

	rf.state = FOLLOWER

	rf.currentTerm = 0
	rf.votedFor = -1
	rf.log = make([]Log, 0, 10)

	rf.commitIndex = 0
	rf.lastApplied = 0

	// For LEADER
	rf.nextIndex = make([]int, servers)
	rf.matchIndex = make([]int, servers)
	for i := 0; i < servers; i++ {
		rf.nextIndex[i] = len(rf.log) + 1
		rf.matchIndex[i] = 0
	}

	go rf.Election()
	go rf.Heartbeat()
	go rf.ApplyMessage(applyCh)

	return rf
}
