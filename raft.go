package raft

//
// this is an outline of the API that raft must expose to
// the service (or tester). see comments below for
// each of these functions for more details.
//
// rf = Make(...)
//   create a new Raft server.
// rf.Start(command interface{}) (index, term, isleader)
//   start agreement on a new log entry
// rf.GetState() (term, isLeader)
//   ask a Raft for its current term, and whether it thinks it is leader
// ApplyMsg
//   each time a new entry is committed to the log, each Raft peer
//   should send an ApplyMsg to the service (or tester)
//   in the same server.
//

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)
import "labrpc"

// import "bytes"
// import "labgob"

//
// as each Raft peer becomes aware that successive log entries are
// committed, the peer should send an ApplyMsg to the service (or
// tester) on the same server, via the applyCh passed to Make(). set
// CommandValid to true to indicate that the ApplyMsg contains a newly
// committed log entry.
//
// in Lab 3 you'll want to send other kinds of messages (e.g.,
// snapshots) on the applyCh; at that point you can add fields to
// ApplyMsg, but set CommandValid to false for these other uses.
//
type ApplyMsg struct {
	CommandValid bool
	Command      interface{}
	CommandIndex int
	CommandTerm  int
}

//
// A Go object implementing a single Raft peer.
//
type Raft struct {
	mu        sync.Mutex          // Lock to protect shared access to this peer's state
	peers     []*labrpc.ClientEnd // RPC end points of all peers
	persister *Persister          // Object to hold this peer's persisted state
	me        int                 // this peer's index into peers[]

	// Your data here (Lab1, Lab2, Challenge1).
	// Look at the paper's Figure 2 for a description of what
	// state a Raft server must maintain.

	status Status // server state (follower, candidate, or leader)

	voteCount int // count of votes for the leader election

	// Persistent state on all servers
	currentTerm int   // latest term server has seen
	votedFor    int   // candidadeId that received vote in current term
	log         []Log // log entries

	// Volatile state on all servers
	commitIndex int // index of highest log entry known to be committed
	lastApplied int // index of highest log entry applied to state machine

	// Volatile state on leader
	leaderState LeaderState

	// Channels requiered for server notification
	electionWonCh chan int
	heartbeatCh   chan int
	applyCh       chan ApplyMsg

	// For debugging purpose
	debug bool
}

func (rf *Raft) resetAsFollower(term int) {
	rf.status = FOLLOWER
	rf.currentTerm = term
	rf.votedFor = -1

	if rf.debug {
		fmt.Printf("[Server %d]: becomes follower\n", rf.me)
	}
}

type Status int

const (
	FOLLOWER  Status = 0
	CANDIDATE Status = 1
	LEADER    Status = 2
)

type Log struct {
	Term    int
	Command interface{}
}

type LeaderState struct {
	nextIndex  []int // for each server, index of the next log entry to send to that server
	matchIndex []int // for each server, index of highest log entry to be replicated on server
}

// return currentTerm and whether this server
// believes it is the leader.
func (rf *Raft) GetState() (int, bool) {

	var term int
	var isleader bool
	// Your code here (Lab1).
	rf.mu.Lock()
	term = rf.currentTerm
	isleader = rf.status == LEADER
	rf.mu.Unlock()
	return term, isleader
}

//
// save Raft's persistent state to stable storage,
// where it can later be retrieved after a crash and restart.
// see paper's Figure 2 for a description of what should be persistent.
//
func (rf *Raft) persist() {
	// Your code here (Challenge1).

}

//
// restore previously persisted state.
//
func (rf *Raft) readPersist(data []byte) {
	// Your code here (Challenge1).
}

func compareInt(a, b int) int {
	if a > b {
		return 1
	}
	if a < b {
		return -1
	}
	return 0
}

//
// example RequestVote RPC arguments structure.
// field names must start with capital letters!
//
type RequestVoteArgs struct {
	// Your data here (Lab1, Lab2).
	Term         int // candidate's term
	CandidateId  int // candidate requesting vote
	LastLogIndex int // index of candidate's log entry
	LastLogTerm  int // term of candidate's log entry
}

//
// example RequestVote RPC reply structure.
// field names must start with capital letters!
//
type RequestVoteReply struct {
	// Your data here (Lab1).
	Term        int  // cuurentTerm, for candidate to update itself
	VoteGranted bool // true means candidate received vote
}

// Check if the candidate logs are at least as up-to-date as server logs
func (rf *Raft) checkLogs(args *RequestVoteArgs) bool {
	switch compareInt(rf.log[len(rf.log)-1].Term, args.LastLogTerm) {
	case -1:
		return true
	case 0:
		if len(rf.log)-1 <= args.LastLogIndex {
			return true
		}
		fallthrough
	default:
		return false
	}
}

//
// example RequestVote RPC handler.
//
func (rf *Raft) RequestVote(args *RequestVoteArgs, reply *RequestVoteReply) {
	// Your code here (Lab1, Lab2).
	rf.mu.Lock()
	switch compareInt(rf.currentTerm, args.Term) {
	case 1: // candidate outdated
		reply.Term = rf.currentTerm
		reply.VoteGranted = false
	case -1: // server outdated
		rf.resetAsFollower(args.Term)
		fallthrough
	case 0:
		reply.Term = args.Term

		if (rf.votedFor == -1 || rf.votedFor == args.CandidateId) && rf.checkLogs(args) {
			reply.VoteGranted = true
			rf.votedFor = args.CandidateId

			if rf.debug {
				fmt.Printf("[Server %d]: grants vote to %d\n", rf.me, rf.votedFor)
			}
		} else {
			reply.VoteGranted = false
		}
	}
	rf.mu.Unlock()
}

//
// example code to send a RequestVote RPC to a server.
// server is the index of the target server in rf.peers[].
// expects RPC arguments in args.
// fills in *reply with RPC reply, so caller should
// pass &reply.
// the types of the args and reply passed to Call() must be
// the same as the types of the arguments declared in the
// handler function (including whether they are pointers).
//
// The labrpc package simulates a lossy network, in which servers
// may be unreachable, and in which requests and replies may be lost.
// Call() sends a request and waits for a reply. If a reply arrives
// within a timeout interval, Call() returns true; otherwise
// Call() returns false. Thus Call() may not return for a while.
// A false return can be caused by a dead server, a live server that
// can't be reached, a lost request, or a lost reply.
//
// Call() is guaranteed to return (perhaps after a delay) *except* if the
// handler function on the server side does not return.  Thus there
// is no need to implement your own timeouts around Call().
//
// look at the comments in ../labrpc/labrpc.go for more details.
//
// if you're having trouble getting RPC to work, check that you've
// capitalized all field names in structs passed over RPC, and
// that the caller passes the address of the reply struct with &, not
// the struct itself.
//
func (rf *Raft) sendRequestVote(server int, args *RequestVoteArgs, reply *RequestVoteReply) bool {
	ok := rf.peers[server].Call("Raft.RequestVote", args, reply)

	rf.mu.Lock()
	if ok && rf.status == CANDIDATE && rf.currentTerm == args.Term { // check that the response is good and that the server is still in the sending context (candidate of same term)
		switch compareInt(rf.currentTerm, reply.Term) {
		case 1: // wrong election
		case -1: // candidate outdated
			rf.resetAsFollower(reply.Term)
		case 0:
			if reply.VoteGranted {
				rf.voteCount++
				if rf.voteCount > len(rf.peers)/2 {
					rf.status = LEADER
					rf.electionWonCh <- 1 // notify the candidate that he won
					rf.voteCount = 0      // reset the voteCount to avoid the leader to be elected 2 times in the same term

					// Create volatile leader state
					rf.leaderState.nextIndex = make([]int, len(rf.peers))
					rf.leaderState.matchIndex = make([]int, len(rf.peers))

					// Initialize the leaderState to handle RPCs
					for server := range rf.leaderState.nextIndex {
						rf.leaderState.nextIndex[server] = len(rf.log)
						rf.leaderState.matchIndex[server] = 0
					}

					if rf.debug {
						fmt.Printf("[Server %d]: becomes leader\n", rf.me)
					}
				}
			}
		}
	}
	rf.mu.Unlock()

	return ok
}

type AppendEntriesArgs struct {
	Term         int
	LeaderId     int
	PrevLogIndex int
	PrevLogTerm  int
	Entries      []Log
	LeaderCommit int
}

type AppendEntriesReply struct {
	Term      int
	Success   bool
	NextIndex int // Define the NextIndex to send to that server
}

// AppendEntries handler
func (rf *Raft) AppendEntries(args *AppendEntriesArgs, reply *AppendEntriesReply) {
	rf.mu.Lock()

	switch compareInt(rf.currentTerm, args.Term) {
	case 1: // outdated leader
		reply.Term = rf.currentTerm
		reply.NextIndex = len(rf.log)
		reply.Success = false
	case -1: // oudated server
		fallthrough
	case 0: // legitimate heartbeat
		rf.resetAsFollower(args.Term) // reset as follower even if the term is the same to avoid other candidates of same term to be stuck
		reply.Term = rf.currentTerm

		switch compareInt(len(rf.log), args.PrevLogIndex) {
		case -1:
			fallthrough
		case 0: // PrevLogIndex is too high compared to log size
			reply.Success = false
			reply.NextIndex = len(rf.log)

			if rf.debug {
				fmt.Printf("[Server %d]: PrevLogIndex (%d) > last log index (%d), nextIndex = %d\n", rf.me, args.PrevLogIndex, len(rf.log)-1, reply.NextIndex)
			}
		case 1: // PrevLogIndex seems ok
			if rf.log[args.PrevLogIndex].Term == args.PrevLogTerm {
				// Keep all the log entries up to PrevLogIndex
				rf.log = rf.log[0 : args.PrevLogIndex+1]
				// Add the new entries to the log
				rf.log = append(rf.log, args.Entries...)

				// Check if the commitIndex needs to be updated
				if rf.commitIndex < args.LeaderCommit {
					rf.commitIndex = (map[bool]int{true: len(rf.log) - 1, false: args.LeaderCommit})[len(rf.log)-1 <= args.LeaderCommit]
					rf.applyLogToStateMachine()
				}

				reply.Success = true
				reply.NextIndex = args.PrevLogIndex + len(args.Entries) + 1

				if rf.debug {
					fmt.Printf("[Server %d]: %d entries written (from %d to %d)\n", rf.me, len(args.Entries), args.PrevLogIndex+1, reply.NextIndex-1)
				}
			} else {
				index := args.PrevLogIndex
				term := rf.log[args.PrevLogIndex].Term
				// Decreases the nextIndex down to after an entry with a term different from PrevLogIndex
				for ; rf.log[index].Term == term; index-- {
				}

				reply.Success = false
				reply.NextIndex = index + 1

				if rf.debug {
					fmt.Printf("[Server %d]: PrevLogTerm (%d) different from last log term (%d), nextIndex = %d\n", rf.me, args.PrevLogTerm, term, reply.NextIndex)
				}
			}
		}
		rf.heartbeatCh <- 1 // trigger heartbeat reception from leader
	}

	rf.mu.Unlock()
}

// Apply log entries to the state machine
func (rf *Raft) applyLogToStateMachine() {
	if rf.debug {
		fmt.Printf("[Server %d]: apply Log from %d to %d\n", rf.me, rf.lastApplied+1, rf.commitIndex)
	}
	for index := rf.lastApplied + 1; index <= rf.commitIndex; index++ {
		rf.applyCh <- ApplyMsg{CommandIndex: index, CommandValid: true, Command: rf.log[index].Command, CommandTerm: rf.log[index].Term}
	}
	rf.lastApplied = rf.commitIndex
}

// Send an AppendEntries to a server
func (rf *Raft) sendAppendEntries(server int, args *AppendEntriesArgs, reply *AppendEntriesReply) bool {
	ok := rf.peers[server].Call("Raft.AppendEntries", args, reply)

	if ok && rf.status == LEADER && rf.currentTerm == args.Term { // check that the response is good and that the server is still in the sending context (leader of same term)
		rf.mu.Lock()

		switch compareInt(rf.currentTerm, reply.Term) {
		case -1: // outdated leader
			rf.resetAsFollower(reply.Term)
		default:
			if reply.Success {
				if rf.debug {
					fmt.Printf("[Server %d]: NextIndex to send to %d: %d\n", rf.me, server, reply.NextIndex)
				}

				// Update leaderState after receiving successful ACK from server
				rf.leaderState.nextIndex[server] = reply.NextIndex
				rf.leaderState.matchIndex[server] = reply.NextIndex - 1
				// Update the commitIndex
				rf.updateCommitIndex()
			} else {
				if rf.debug {
					fmt.Printf("[Server %d]: error NextIndex to send to %d: from %d to %d\n", rf.me, server, rf.leaderState.nextIndex[server], reply.NextIndex)
				}
				rf.leaderState.nextIndex[server] = reply.NextIndex
			}
		}

		rf.mu.Unlock()
	}
	return ok
}

// Function called by the leader to update its commitIndex
func (rf *Raft) updateCommitIndex() {
	// Update the matchIndex for the leader
	rf.leaderState.matchIndex[rf.me] = len(rf.log) - 1

	// Go through the entries added in the current term and check if they can be committed
	for i := len(rf.log) - 1; i > rf.commitIndex && rf.log[i].Term == rf.currentTerm; i-- {
		nbServers := 0

		for server := range rf.peers {
			if rf.leaderState.matchIndex[server] >= i {
				nbServers++
			}
		}

		if nbServers > len(rf.peers)/2 {
			rf.commitIndex = i
			rf.applyLogToStateMachine()
			break
		}
	}
}

//
// the service using Raft (e.g. a k/v server) wants to start
// agreement on the next command to be appended to Raft's log. if this
// server isn't the leader, returns false. otherwise start the
// agreement and return immediately. there is no guarantee that this
// command will ever be committed to the Raft log, since the leader
// may fail or lose an election.
//
// the first return value is the index that the command will appear at
// if it's ever committed. the second return value is the current
// term. the third return value is true if this server believes it is
// the leader.
//
func (rf *Raft) Start(command interface{}) (int, int, bool) {
	index := -1
	term := -1
	isLeader := true

	// Your code here (Lab2).
	rf.mu.Lock()
	if rf.status == LEADER {
		index = len(rf.log)
		term = rf.currentTerm
		newLog := &Log{Term: rf.currentTerm, Command: command}
		rf.log = append(rf.log, *newLog)

		if rf.debug {
			fmt.Printf("[Server %d]: new log entry w/ index %d, term %d, value %v\n", rf.me, index, term, command)
		}
	} else {
		isLeader = false
	}
	rf.mu.Unlock()

	return index, term, isLeader
}

//
// the tester calls Kill() when a Raft instance won't
// be needed again. you are not required to do anything
// in Kill(), but it might be convenient to (for example)
// turn off debug output from this instance.
//
func (rf *Raft) Kill() {
	// Your code here, if desired.
	rf.debug = false
}

//
// the service or tester wants to create a Raft server. the ports
// of all the Raft servers (including this one) are in peers[]. this
// server's port is peers[me]. all the servers' peers[] arrays
// have the same order. persister is a place for this server to
// save its persistent state, and also initially holds the most
// recent saved state, if any. applyCh is a channel on which the
// tester or service expects Raft to send ApplyMsg messages.
// Make() must return quickly, so it should start goroutines
// for any long-running work.
//
func Make(peers []*labrpc.ClientEnd, me int,
	persister *Persister, applyCh chan ApplyMsg) *Raft {
	rf := &Raft{}
	rf.peers = peers
	rf.persister = persister
	rf.me = me

	// Your initialization code here (Lab1, Lab2, Challenge1).
	rf.status = FOLLOWER
	rf.currentTerm = 0
	rf.votedFor = -1
	EmptyLog := &Log{Term: 0} // empty log used to fill the hole before first real log
	rf.log = append(rf.log, *EmptyLog)

	rf.commitIndex = 0
	rf.lastApplied = 0

	// Create channels to handle notifications in main loop
	rf.heartbeatCh = make(chan int, len(rf.peers))
	rf.electionWonCh = make(chan int, len(rf.peers))
	rf.applyCh = applyCh

	// Enable/Disable debugging
	rf.debug = false

	// infinite loop running on top of
	go func(rf *Raft) {
		for {
			if rf.debug {
				fmt.Printf("[Server %d]: state %d, term %d, commitIndex %d, log %v\n", rf.me, rf.status, rf.currentTerm, rf.commitIndex, rf.log)
			}
			switch rf.status {
			case FOLLOWER:
				timer := time.NewTimer(ElectionTimeout())
				select {
				case <-rf.heartbeatCh: // meaning that leader is alive
				case <-timer.C: // becomes a candidate
					rf.mu.Lock()
					rf.status = CANDIDATE

					if rf.debug {
						fmt.Printf("[Server %d]: becomes candidate\n", rf.me)
					}
					rf.mu.Unlock()
				}
			case CANDIDATE:
				// Init candidate state
				rf.mu.Lock()
				rf.currentTerm++
				rf.votedFor = rf.me
				rf.voteCount = 1
				rf.mu.Unlock()

				// Send requestvotes to other servers
				go func(rf *Raft) {
					// Build args to send
					args := &RequestVoteArgs{
						Term:         rf.currentTerm,
						CandidateId:  rf.me,
						LastLogIndex: len(rf.log) - 1,
						LastLogTerm:  rf.log[len(rf.log)-1].Term,
					}

					for server := range rf.peers {
						if server != rf.me {
							go rf.sendRequestVote(server, args, &RequestVoteReply{}) // create a different reply for each RPC
						}
					}
				}(rf)

				timer := time.NewTimer(ElectionTimeout())
				select {
				case <-rf.heartbeatCh: // meaning that he became a follower
				case <-rf.electionWonCh: // meaning that he became a leader
				case <-timer.C: // need to start a new election
				}
			case LEADER:
				// Send heartbeats to other servers
				go func(rf *Raft) {
					for server := range rf.peers {
						if server != rf.me {
							/*if rf.leaderState.nextIndex[server] <= 0 {
								fmt.Printf("[Server %d]: error nextindex[%d]=%d\n", rf.me, server, rf.leaderState.nextIndex[server])
							}*/
							rf.mu.Lock()
							args := &AppendEntriesArgs{ // create different args for each server
								Term:         rf.currentTerm,
								LeaderId:     rf.me,
								PrevLogIndex: rf.leaderState.nextIndex[server] - 1,
								PrevLogTerm:  rf.log[rf.leaderState.nextIndex[server]-1].Term,
								Entries:      rf.log[rf.leaderState.nextIndex[server]:len(rf.log)],
								LeaderCommit: rf.commitIndex,
							}
							rf.mu.Unlock()
							go rf.sendAppendEntries(server, args, &AppendEntriesReply{}) // create a different reply for each RPC
						}
					}
				}(rf)

				time.Sleep(heartbeatTimeout)
			}
		}
	}(rf)

	return rf
}

const (
	heartbeatTimeout              = 100 * time.Millisecond // less than 10 heartbeats per second
	electionTimeoutLowerBound int = 400
	electionTimeoutUpperBound int = 1000
)

func ElectionTimeout() time.Duration {
	return time.Duration(electionTimeoutLowerBound+rand.Intn(electionTimeoutUpperBound-electionTimeoutLowerBound)) * time.Millisecond
}
