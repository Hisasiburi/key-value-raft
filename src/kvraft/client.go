package raftkv

import (
	"crypto/rand"
	"fmt"
	"labrpc"
	"math/big"
	"sync"
	"time"
)

type Clerk struct {
	servers []*labrpc.ClientEnd
	// You will have to modify this struct.
	me       int64
	mu       sync.Mutex
	leaderId int
}

func nrand() int64 {
	max := big.NewInt(int64(1) << 62)
	bigx, _ := rand.Int(rand.Reader, max)
	x := bigx.Int64()
	return x
}

func MakeClerk(servers []*labrpc.ClientEnd) *Clerk {
	ck := new(Clerk)
	ck.servers = servers
	// You'll have to add code here.
	ck.leaderId = -1
	ck.me = nrand()
	return ck
}

// For debugging
func (ck *Clerk) print(field string, value interface{}) {
	if !debugging {
		return
	}
	fmt.Print("[CLIENT ] ", field, " = ", value, "\n")
}

//
// fetch the current value for a key.
// returns "" if the key does not exist.
// keeps trying forever in the face of all other errors.
//
// you can send an RPC with code like this:
// ok := ck.servers[i].Call("KVServer.Get", &args, &reply)
//
// the types of args and reply (including whether they are pointers)
// must match the declared types of the RPC handler function's
// arguments. and reply must be passed as a pointer.
//
func (ck *Clerk) Get(key string) string {
	ck.mu.Lock()
	// You will have to modify this function.
	OperationId := nrand()
	args := GetArgs{}
	args.Key = key
	args.ClientId = ck.me
	args.OperationId = OperationId
	reply := GetReply{}
	// reply.WrongLeader = true
	// reply.Err = ErrNoKey
	length := len(ck.servers)
	i := length / 2 // TODO
	if ck.leaderId != -1 {
		i = ck.leaderId
	}
	ok := ck.servers[i].Call("KVServer.Get", &args, &reply)
	if ok && !reply.WrongLeader {
		ck.leaderId = i
	} else {
		ck.leaderId = -1
	}
	if ok && reply.Err == ErrNoKey {
		ck.mu.Unlock()
		return ""
	}
	for !(ok && !reply.WrongLeader) {
		time.Sleep(50 * time.Millisecond)
		i++
		i = i % length
		args = GetArgs{}
		args.Key = key
		args.ClientId = ck.me
		args.OperationId = OperationId
		reply = GetReply{}
		if ck.leaderId != -1 {
			i = ck.leaderId
		}
		ok = ck.servers[i].Call("KVServer.Get", &args, &reply)
		if ok && !reply.WrongLeader {
			ck.leaderId = i
		} else {
			ck.leaderId = -1
		}
		if ok && reply.Err == Duplicate {
			ck.mu.Unlock()
			return reply.Value
		}
		if ok && reply.Err == ErrNoKey {
			ck.mu.Unlock()
			return ""
		}
	}
	ck.mu.Unlock()
	return reply.Value
}

//
// shared by Put and Append.
//
// you can send an RPC with code like this:
// ok := ck.servers[i].Call("KVServer.PutAppend", &args, &reply)
//
// the types of args and reply (including whether they are pointers)
// must match the declared types of the RPC handler function's
// arguments. and reply must be passed as a pointer.
//
func (ck *Clerk) PutAppend(key string, value string, op string) {
	ck.mu.Lock()
	// You will have to modify this function.
	OperationId := nrand()
	args := PutAppendArgs{}
	args.ClientId = ck.me
	args.OperationId = OperationId
	args.Key = key
	args.Value = value
	args.Op = op
	reply := PutAppendReply{}
	// reply.WrongLeader = true
	// reply.Err = ErrNoKey
	length := len(ck.servers)
	i := length / 2 // TODO
	if ck.leaderId != -1 {
		i = ck.leaderId
	}
	ok := ck.servers[i].Call("KVServer.PutAppend", &args, &reply)
	if ok && !reply.WrongLeader {
		ck.leaderId = i
	} else {
		ck.leaderId = -1
	}
	if ok && reply.Err == Duplicate {
		ck.mu.Unlock()
		return
	}
	for !(ok && !reply.WrongLeader) {
		time.Sleep(50 * time.Millisecond)
		i++
		i = i % length
		args = PutAppendArgs{}
		args.ClientId = ck.me
		args.OperationId = OperationId
		args.Key = key
		args.Value = value
		args.Op = op
		reply = PutAppendReply{}
		if ck.leaderId != -1 {
			i = ck.leaderId
		}
		ok = ck.servers[i].Call("KVServer.PutAppend", &args, &reply)
		if ok && !reply.WrongLeader {
			ck.leaderId = i
		} else {
			ck.leaderId = -1
		}
		if ok && reply.Err == Duplicate {
			ck.mu.Unlock()
			return
		}
	}
	ck.mu.Unlock()
}

func (ck *Clerk) Put(key string, value string) {
	ck.PutAppend(key, value, "Put")
}
func (ck *Clerk) Append(key string, value string) {
	ck.PutAppend(key, value, "Append")
}
