package raftkv

import (
	"fmt"
	"labgob"
	"labrpc"
	"log"
	"raft"
	"sync"
	"time"
)

const Debug = 0
const TIMEOUT = 100

func DPrintf(format string, a ...interface{}) (n int, err error) {
	if Debug > 0 {
		log.Printf(format, a...)
	}
	return
}

type Op struct {
	// Your definitions here.
	// Field names must start with capital letters,
	// otherwise RPC will break.
	Oid int64
	Fun string
	Key string
	Val string
}

type KVServer struct {
	mu      sync.Mutex
	me      int
	rf      *raft.Raft
	applyCh chan raft.ApplyMsg

	maxraftstate int // snapshot if log grows this big

	// Your definitions here.
	store   []Record
	results []Result
}

// For debugging
func (kv *KVServer) print(field string, value interface{}) {
	// if field == "PutAppend args" || field == "kv.results[index-1].fun Get?" || field == "kv.results[index-1].fun" {
	// 	fmt.Print("[SERVER", kv.me, "] ", field, " = ", value, "\n")
	// 	return
	// }
	if !debugging {
		return
	}
	fmt.Print("[SERVER", kv.me, "] ", field, " = ", value, "\n")
}

func (kv *KVServer) Get(args *GetArgs, reply *GetReply) {
	// Your code here.
	op := Op{
		Oid: args.OperationId,
		Fun: "Get",
		Key: args.Key,
		Val: "",
	}
	index, term, isLeader := kv.rf.Start(op)
	if !isLeader {
		reply.WrongLeader = true
		reply.Err = NotLeader
		reply.Value = ""
		return
	}
	kv.mu.Lock()
	length := len(kv.results)
	kv.mu.Unlock()
	count := 0
	kv.print("loop", "start")
	for length < index && count < TIMEOUT {
		time.Sleep(10 * time.Millisecond)
		kv.mu.Lock()
		length = len(kv.results)
		kv.mu.Unlock()
		count++
	}
	kv.print("loop", "end")
	if count >= TIMEOUT {
		reply.WrongLeader = true
		reply.Err = NotLeader
		reply.Value = ""
		return
	}
	kv.mu.Lock()
	if kv.results[index-1].dup {
		reply.WrongLeader = false
		reply.Err = Duplicate
		reply.Value = kv.results[index-1].val
		kv.mu.Unlock()
		return
	}
	if kv.results[index-1].fun != "Get" || kv.results[index-1].trm != term {
		kv.print("kv.results[index-1].fun Get?", kv.results[index-1].fun)
		reply.WrongLeader = true
		reply.Err = NotLeader
		reply.Value = ""
		kv.mu.Unlock()
		return
	}
	reply.WrongLeader = false
	if kv.results[index-1].err {
		reply.Err = ErrNoKey
	} else {
		reply.Err = OK
	}
	reply.Value = kv.results[index-1].val
	kv.mu.Unlock()
}

func (kv *KVServer) PutAppend(args *PutAppendArgs, reply *PutAppendReply) {
	kv.print("PutAppend args", args)
	// Your code here.
	op := Op{
		Oid: args.OperationId,
		Fun: args.Op,
		Key: args.Key,
		Val: args.Value,
	}
	index, term, isLeader := kv.rf.Start(op)
	if !isLeader {
		reply.WrongLeader = true
		reply.Err = NotLeader
		return
	}
	kv.mu.Lock()
	length := len(kv.results)
	kv.mu.Unlock()
	count := 0
	kv.print("loop", "start")
	for length < index && count < TIMEOUT {
		time.Sleep(10 * time.Millisecond)
		kv.mu.Lock()
		length = len(kv.results)
		kv.mu.Unlock()
		count++
	}
	kv.print("loop", "end")
	if count >= TIMEOUT {
		reply.WrongLeader = true
		reply.Err = NotLeader
		return
	}
	kv.mu.Lock()
	kv.print("kv.results[index-1].dup", kv.results[index-1].dup)
	if kv.results[index-1].dup {
		reply.WrongLeader = false
		reply.Err = Duplicate
		kv.mu.Unlock()
		return
	}
	if kv.results[index-1].fun != args.Op || kv.results[index-1].trm != term {
		kv.print("kv.results[index-1].fun", kv.results[index-1].fun)
		kv.print("args.Op", args.Op)
		reply.WrongLeader = true
		reply.Err = NotLeader
		kv.mu.Unlock()
		return
	}
	reply.WrongLeader = false
	if kv.results[index-1].err {
		reply.Err = ErrNoKey
	} else {
		reply.Err = OK
	}
	kv.mu.Unlock()
}

//
// the tester calls Kill() when a KVServer instance won't
// be needed again. you are not required to do anything
// in Kill(), but it might be convenient to (for example)
// turn off debug output from this instance.
//
func (kv *KVServer) Kill() {
	kv.mu.Lock()
	kv.rf.Kill()
	// Your code here, if desired.
	kv.store = make([]Record, 0, 10)
	kv.results = make([]Result, 0, 10)
	kv.mu.Unlock()
}

//
// servers[] contains the ports of the set of
// servers that will cooperate via Raft to
// form the fault-tolerant key/value service.
// me is the index of the current server in servers[].
// the k/v server should store snapshots with persister.SaveSnapshot(),
// and Raft should save its state (including log) with persister.SaveRaftState().
// the k/v server should snapshot when Raft's saved state exceeds maxraftstate bytes,
// in order to allow Raft to garbage-collect its log. if maxraftstate is -1,
// you don't need to snapshot.
// StartKVServer() must return quickly, so it should start goroutines
// for any long-running work.
//
func StartKVServer(servers []*labrpc.ClientEnd, me int, persister *raft.Persister, maxraftstate int) *KVServer {
	// call labgob.Register on structures you want
	// Go's RPC library to marshall/unmarshall.
	labgob.Register(Op{})

	kv := new(KVServer)
	kv.me = me
	kv.maxraftstate = maxraftstate

	// You may need initialization code here.

	kv.applyCh = make(chan raft.ApplyMsg)
	kv.rf = raft.Make(servers, me, persister, kv.applyCh)

	// You may need initialization code here.
	kv.store = make([]Record, 0, 10)
	kv.results = make([]Result, 0, 10)

	// handle committed commands
	go func() {
		for {
			applyMsg := <-kv.applyCh
			kv.mu.Lock()
			op, ok := applyMsg.Command.(Op)
			index := applyMsg.CommandIndex
			term := applyMsg.CommandTerm
			if !ok {
				kv.results = append(kv.results, Result{
					oid: op.Oid,
					idx: index,
					trm: term,
					fun: op.Fun,
					val: "",
					err: true,
					dup: false,
				})
				kv.mu.Unlock()
				continue
			}
			if isDuplicate, originalValue := kv.duplicate(op.Oid); isDuplicate {
				kv.results = append(kv.results, Result{
					oid: op.Oid,
					idx: index,
					trm: term,
					fun: op.Fun,
					val: originalValue,
					err: true,
					dup: true, // TODO
				})
				kv.mu.Unlock()
				continue
			}
			kv.print("op", op)
			base := ""
			switch op.Fun {
			case "Get":
				value := kv.get(op.Key)
				err := false
				if value == "" {
					err = true
				}
				kv.results = append(kv.results, Result{
					oid: op.Oid,
					idx: index,
					trm: term,
					fun: op.Fun,
					val: value,
					err: err,
					dup: false,
				})
			case "Append":
				base = kv.get(op.Key)
				fallthrough
			case "Put":
				kv.print("base", base)
				kv.print("value", op.Val)
				new := base + op.Val
				kv.print("new", new)
				kv.set(op.Key, new)
				kv.results = append(kv.results, Result{
					oid: op.Oid,
					idx: index,
					trm: term,
					fun: op.Fun,
					val: "",
					err: false,
					dup: false,
				})
			}
			kv.mu.Unlock()
		}
	}()

	return kv
}

type Record struct {
	key string
	val string
}

type Result struct {
	oid int64
	idx int
	trm int
	fun string
	val string
	err bool
	dup bool // TODO
}

func (kv *KVServer) get(key string) string {
	for _, record := range kv.store {
		if record.key == key {
			return record.val
		}
	}
	return ""
}

func (kv *KVServer) set(key string, value string) {
	for i, record := range kv.store {
		if record.key == key {
			kv.store[i].val = value
			return
		}
	}
	kv.store = append(kv.store, Record{
		key: key,
		val: value,
	})
	return
}

func (kv *KVServer) duplicate(Oid int64) (bool, string) {
	for _, result := range kv.results {
		if result.oid == Oid {
			return true, result.val
		}
	}
	return false, ""
}
